package com.nurunabiyev.matchup

object Constants {

    const val PLAYER_ID = "myid"
    const val DIFFICULTY = "difficulty"
    const val ONE_SECOND = 1000
    const val SHOW_INIT_TILES_SEC = 5 * ONE_SECOND
    const val UNSPLASH_CLIENT_ID = "6d696528b126c5a4d8df2abbaa7b1a8a0dc2cff46491cbe038fb81bee6a014ea"
    const val QUERY_CATS = "cats"

    enum class OpenTileResult {
        OPEN_FIRST, OPEN_SECOND,
        FAIL_UNMATCH, FAIL_SAME,
        UNKNOWN
    }

    enum class DifficultyLevels {
        EASY, // 3x4
        MEDIUM, // 4x4
        HARD // 6x4
    }

    enum class GameFinishedResult {
        LEVEL_UNLOCKED, FINISH_NORMAL, NEW_RECORD,
        HARD_LEVEL_PASSED
    }
}
