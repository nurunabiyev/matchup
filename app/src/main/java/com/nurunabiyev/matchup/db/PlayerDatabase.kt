package com.nurunabiyev.matchup.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.nurunabiyev.matchup.game.model.Player

@Database(entities = arrayOf(Player::class), version = 1)
abstract class PlayerDatabase : RoomDatabase() {
    abstract fun playerDao(): PlayerDao

    companion object {
        private var INSTANCE: PlayerDatabase? = null

        fun getInstance(context: Context): PlayerDatabase? {
            if (INSTANCE == null) {
                synchronized(PlayerDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            PlayerDatabase::class.java, "player.db")
                            .allowMainThreadQueries()   // for simplicity
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}