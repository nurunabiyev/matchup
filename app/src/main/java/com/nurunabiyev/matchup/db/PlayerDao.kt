package com.nurunabiyev.matchup.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.IGNORE
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import com.nurunabiyev.matchup.game.model.Player

/**
 * Data access object for player
 */
@Dao
interface PlayerDao {

    @Query("SELECT * from Player where id = :id")
    fun lookPlayerById(id: String): LiveData<Player>

    @Query("SELECT * from Player")
    fun findAllPlayers(): List<Player>

    @Update(onConflict = REPLACE)
    fun updatePlayer(player: Player)

    @Insert(onConflict = IGNORE)
    fun insertPlayer(player: Player)

    @Query("DELETE FROM Player")
    fun deleteAll()
}
