package com.nurunabiyev.matchup.game.model

/**
 * Holds tiles only
 */
class GameBoard(val tiles: ArrayList<Tile>) {

    fun getTile(pos: Int): Tile {
        return tiles[pos]
    }

    fun setPhotoToTiles(photoUrl: String) {
        var curUuid: String? = null
        for (tile: Tile in tiles) {
            if (curUuid != null && tile.uuid == curUuid) {
                tile.photoUrl = photoUrl
                break
            }
            if (tile.photoUrl == null && curUuid == null) {
                tile.photoUrl = photoUrl
                curUuid = tile.uuid
            }
        }
    }

    /**
     * If at least one tile is not open, will return false
     */
    fun didOpenAllTiles(): Boolean {
        for (tile: Tile in tiles)
            if (!tile.isOpen)
                return false
        return true
    }
}
