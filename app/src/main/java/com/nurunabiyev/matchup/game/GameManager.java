package com.nurunabiyev.matchup.game;

import android.arch.lifecycle.MutableLiveData;

import com.nurunabiyev.matchup.Constants.OpenTileResult;
import com.nurunabiyev.matchup.game.model.GameBoard;
import com.nurunabiyev.matchup.game.model.Tile;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

public class GameManager {

    private GameBoard gameBoard;
    private Tile prevOpenedTile;
    public MutableLiveData<Boolean> didPlayerWin;
    public boolean gameStarted = true;

    public GameManager(int size) {
        gameBoard = new GameBoard(createRandomTiles(size));
        prevOpenedTile = null;
        didPlayerWin = new MutableLiveData<>();
    }

    public GameBoard getGameBoard() {
        return gameBoard;
    }

    /**
     * Attempts to open the tile, returns whether the player won or not
     *
     * @param pos, tile to open
     * @return the result of this try
     */
    public OpenTileResult openTile(int pos) {
        OpenTileResult openResult;

        // if its same as previous - ignore
        if (gameBoard.getTile(pos).isOpen()) {
            return OpenTileResult.FAIL_SAME;
        }

        // if a previously opened tile is null, it is 1st tile. Show it and don't close
        if (prevOpenedTile == null) {
            gameBoard.getTile(pos).setOpen(true);
            prevOpenedTile = gameBoard.getTile(pos);
            openResult = OpenTileResult.OPEN_FIRST;
        }

        // if prev is not null, it is second tile
        else {
            Tile curTile = gameBoard.getTile(pos);
            // if ids of this and prev matches, we found a match
            if (curTile.getUuid().equals(prevOpenedTile.getUuid())) {
                // keep them opened, prevent from opening / closing again
                gameBoard.getTile(pos).setOpen(true);
                openResult = OpenTileResult.OPEN_SECOND;
            } else {
                // close current and previous
                gameBoard.getTile(pos).setOpen(false);
                prevOpenedTile.setOpen(false);
                openResult = OpenTileResult.FAIL_UNMATCH;
            }

            // prev is null
            prevOpenedTile = null;
        }

        if (gameBoard.didOpenAllTiles())
            didPlayerWin.postValue(true);

        return openResult;
    }

    /**
     * Opens or closes all tiles in the gameboard
     */
    public void flipAllTiles(boolean opened) {
        for (Tile tile : gameBoard.getTiles()) {
            tile.setOpen(opened);
        }
    }

    private ArrayList<Tile> createTestTiles(int size) {
        ArrayList<Tile> tiles = new ArrayList<>();

        int id = 0;
        for (int i = 0; i < size; i++) {
            if (i % 2 != 0) {
                id++;
            }
            Tile newTile = new Tile(false, String.valueOf(id));
            newTile.setPosition(i);
            tiles.add(newTile);
        }
        tiles.get(size - 1).setUuid(tiles.get(0).getUuid());

        return tiles;
    }

    /**
     * Gets unique tiles and randomly places to the board.
     */
    private ArrayList<Tile> createRandomTiles(int size) {
        // only random tiles (they are 2 times less total number of tiles on the board)
        int uniqueTilesNum = size / 2;
        ArrayList<Tile> uniqueTiles = new ArrayList<>();
        for (int i = 0; i < uniqueTilesNum; i++) {
            uniqueTiles.add(new Tile(false, UUID.randomUUID().toString()));
        }

        ArrayList<Tile> boardTiles = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            Tile nextRand = getNextUniqueTile(uniqueTiles);

            if (nextRand != null) {
                Tile curTile = new Tile(false, nextRand.getUuid());
                curTile.setPosition(i);
                boardTiles.add(curTile);
            }

            // cleanup
            if (nextRand != null && !nextRand.isOpen())
                uniqueTiles.get(uniqueTiles.indexOf(nextRand)).setOpen(true);
            else if (nextRand != null && nextRand.isOpen())
                uniqueTiles.set(uniqueTiles.indexOf(nextRand), null);
        }

        return boardTiles;
    }

    private Tile getNextUniqueTile(ArrayList<Tile> uniqueTiles) {
        Tile randomTile;
        Random rand = new Random();
        int randIndex = rand.nextInt(uniqueTiles.size());

        while (uniqueTiles.get(randIndex) == null) {
            randIndex = rand.nextInt(uniqueTiles.size());
        }

        randomTile = uniqueTiles.get(randIndex);
        if (randomTile != null) {
            return randomTile;
        }
        return null;
    }
}
