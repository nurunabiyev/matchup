package com.nurunabiyev.matchup.game.model

/**
 * Each tile has a opened or closed state and uniques id
 */
class Tile(var isOpen: Boolean, var uuid: String) {
    var position = -1
    var photoUrl : String? = null
}