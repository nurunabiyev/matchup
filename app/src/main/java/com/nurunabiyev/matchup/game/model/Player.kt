package com.nurunabiyev.matchup.game.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * POJO
 */
@Entity
class Player(@PrimaryKey var id: String,
             var username: String) {
    var easyScore: Int = 0
    var mediumScore: Int = 0
    var hardScore: Int = 0
}
