package com.nurunabiyev.matchup.view;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.kc.unsplash.models.Photo;
import com.nurunabiyev.matchup.Constants;
import com.nurunabiyev.matchup.R;
import com.nurunabiyev.matchup.game.GameManager;
import com.nurunabiyev.matchup.game.model.Tile;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import eu.davidea.flipview.FlipView;

import static com.nurunabiyev.matchup.Constants.ONE_SECOND;
import static com.nurunabiyev.matchup.Constants.OpenTileResult.UNKNOWN;
import static com.nurunabiyev.matchup.Constants.SHOW_INIT_TILES_SEC;

public class TilesAdapter extends BaseAdapter {

    private GameManager mGameManager;
    private Context context;
    private ArrayList<Tile> tiles;
    private ArrayList<Photo> photos;
    private TileHolder prevOpenedTile = null;
    private boolean canPress = true; // to suppress pressing the tiles
    private MutableLiveData<Long> mElapsedTime = new MutableLiveData<>();
    private long mInitialTime;
    private TimerTask timerTask;

    TilesAdapter(final Context context, GameManager gameManager, List<Photo> results) {
        this.context = context;
        mGameManager = gameManager;
        this.tiles = mGameManager.getGameBoard().getTiles();
        this.photos = (ArrayList<Photo>) results;
        timerTask = new TimerTask() {
            @Override
            public void run() {
                final long newValue = (SystemClock.elapsedRealtime() - mInitialTime) / ONE_SECOND;
                mElapsedTime.postValue(newValue);
            }
        };

        startTimeCounter();
    }

    @Override
    public int getCount() {
        return tiles.size();
    }

    @Override
    public Object getItem(int i) {
        return tiles.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public MutableLiveData<Long> getElapsedTime() {
        return mElapsedTime;
    }

    public void stopTimer() {
        timerTask.cancel();
    }

    private void startTimeCounter() {
        mInitialTime = SystemClock.elapsedRealtime() + SHOW_INIT_TILES_SEC - ONE_SECOND;
        Timer timer = new Timer();

        // Update the elapsed time every second.
        timer.scheduleAtFixedRate(timerTask, SHOW_INIT_TILES_SEC, ONE_SECOND);
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        TileHolder viewHolder;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.item_tile, viewGroup, false);
            LinearLayout tileLayout = view.findViewById(R.id.tileLayout);
            viewHolder = new TileHolder(tileLayout, tiles.get(position), photos.get(position / 2));
            view.setTag(viewHolder);
        }

        viewHolder = (TileHolder) view.getTag();
        viewHolder.bindView();

        return view;
    }

    private class TileHolder {
        FlipView flipView;
        LinearLayout tileLayout;
        Tile tile;
        Photo photo;
        Handler gameStartedHandler = new Handler();
        Runnable gameStartedRunnable;
        Handler showUnmatchedHandler = new Handler();
        Runnable showUnmatched;

        TileHolder(LinearLayout tileLayout, final Tile tile, final Photo photo) {
            this.tileLayout = tileLayout;
            flipView = tileLayout.findViewById(R.id.flipView);
            this.tile = tile;
            this.photo = photo;

            Glide.with(context)
                    .asBitmap()
                    .load(tile.getPhotoUrl())
                    .apply(RequestOptions.circleCropTransform())
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            flipView.setRearImageBitmap(resource);
                        }
                    });

            gameStartedRunnable = new Runnable() {
                @Override
                public void run() {
                    flipTile(false);
                    mGameManager.gameStarted = false;
                    canPress = true;
                }
            };

            showUnmatched = new Runnable() {
                public void run() {
                    flipTile(false);
                    closePrevTile();
                    canPress = true;
                }
            };
        }

        void bindView() {
            if (!mGameManager.gameStarted)
                flipTile(tile.isOpen());
            else
                flipTile(true);

            if (mGameManager.gameStarted) {
                showGameStarted();
            }

            addClickListener();
        }

        private void addClickListener() {
            tileLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Constants.OpenTileResult result = UNKNOWN;
                    if (canPress) {
                        result = mGameManager.openTile(tile.getPosition());
                    }
                    switch (result) {
                        case FAIL_SAME:
                            // do nothing
                        case UNKNOWN:
                            // do nothing
                            break;
                        case OPEN_FIRST:
                            flipTile(true);
                            prevOpenedTile = TileHolder.this;
                            break;
                        case OPEN_SECOND:
                            flipTile(true);
                            break;
                        case FAIL_UNMATCH:
                            canPress = false;
                            // open both then close them
                            flipTile(true);
                            showUnmatchedHandler.postDelayed(showUnmatched, 2000);
                            break;
                    }
                }
            });
        }

        private void showGameStarted() {
            canPress = false;
            gameStartedHandler.postDelayed(gameStartedRunnable, SHOW_INIT_TILES_SEC);
        }

        private void flipTile(boolean open) {
            flipView.flip(open);
        }

        private void closePrevTile() {
            if (prevOpenedTile != null) {
                prevOpenedTile.flipView.flip(false);
            }
            prevOpenedTile = null;
        }
    }
}
