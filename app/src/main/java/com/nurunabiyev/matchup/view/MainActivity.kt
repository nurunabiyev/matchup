package com.nurunabiyev.matchup.view

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatButton
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.nurunabiyev.matchup.Constants
import com.nurunabiyev.matchup.Constants.DifficultyLevels
import com.nurunabiyev.matchup.Constants.PLAYER_ID
import com.nurunabiyev.matchup.R
import com.nurunabiyev.matchup.db.PlayerDatabase
import com.nurunabiyev.matchup.game.model.Player
import eu.davidea.flipview.FlipView


class MainActivity : AppCompatActivity() {

    private lateinit var mPlayer: Player
    private lateinit var mDatabase: PlayerDatabase
    // views
    private lateinit var profileFlipView: FlipView
    private lateinit var nameTextView: TextView
    private lateinit var easyRecordTextView: TextView
    private lateinit var mediumRecordTextView: TextView
    private lateinit var hardRecordTextView: TextView
    private lateinit var mediumStart: Button
    private lateinit var hardStart: AppCompatButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mDatabase = PlayerDatabase.getInstance(this)!!

        // record TV
        easyRecordTextView = findViewById(R.id.tv_easy_record)
        mediumRecordTextView = findViewById(R.id.tv_medium_record)
        hardRecordTextView = findViewById(R.id.tv_hard_record)
        // profile picture
        profileFlipView = findViewById(R.id.profile_fv)
        profileFlipView.flip(true)
        // username
        nameTextView = findViewById(R.id.username_tv)
        nameTextView.setOnClickListener { changeUserName() }
        // starting games
        val easyStart = findViewById<AppCompatButton>(R.id.start_easy)
        mediumStart = findViewById(R.id.start_medium)
        hardStart = findViewById(R.id.start_hard)
        easyStart.setOnClickListener { startButtonClicked(DifficultyLevels.EASY) }
        mediumStart.setOnClickListener { startButtonClicked(DifficultyLevels.MEDIUM) }
        hardStart.setOnClickListener { startButtonClicked(DifficultyLevels.HARD) }

        initPlayer()
    }

    private fun initPlayer() {
        val foundPlayer: Player? = mDatabase.playerDao().lookPlayerById(PLAYER_ID).value

        if (foundPlayer == null) {
            // this is first start, create a player
            val newPlayer = Player(PLAYER_ID, "themobilecompany")
            mDatabase.playerDao().insertPlayer(newPlayer)
        }

        mDatabase.playerDao().lookPlayerById(PLAYER_ID).observe(this, Observer {
            mPlayer = it!!
            nameTextView.text = mPlayer.username
            easyRecordTextView.text = String.format(resources.getString(R.string.x_seconds), mPlayer.easyScore.toString())
            mediumRecordTextView.text = String.format(resources.getString(R.string.x_seconds), mPlayer.mediumScore.toString())
            hardRecordTextView.text = String.format(resources.getString(R.string.x_seconds), mPlayer.hardScore.toString())

            if (mPlayer.easyScore == 0) {
                mediumStart.backgroundTintList = this.resources.getColorStateList(R.color.gray)
                mediumStart.isClickable = false
                mediumStart.text = getString(R.string.locked)
            } else {
                mediumStart.backgroundTintList = this.resources.getColorStateList(R.color.colorAccent)
                mediumStart.isClickable = true
                mediumStart.text = getString(R.string.start)
            }

            if (mPlayer.mediumScore == 0) {
                hardStart.backgroundTintList = this.resources.getColorStateList(R.color.gray)
                hardStart.isClickable = false
                hardStart.text = getString(R.string.locked)
            } else {
                hardStart.backgroundTintList = this.resources.getColorStateList(R.color.colorAccent)
                hardStart.isClickable = true
                hardStart.text = getString(R.string.start)
            }

        })
    }

    private fun changeUserName() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Enter new username")
        val view = layoutInflater.inflate(R.layout.dialog_new_username, null)
        val categoryEditText = view.findViewById(R.id.categoryEditText) as EditText
        builder.setView(view)

        builder.setPositiveButton(android.R.string.ok) { dialog, p1 ->
            val newCategory = categoryEditText.text
            var isValid = true
            if (newCategory.isBlank()) {
                categoryEditText.error = getString(R.string.error_invalid)
                isValid = false
            }

            if (isValid) {
                mPlayer.username = categoryEditText.text.toString()
                mDatabase.playerDao().updatePlayer(mPlayer)
            }

            if (!isValid) {
                Toast.makeText(this, R.string.error_invalid, Toast.LENGTH_SHORT).show()
            }

            dialog.dismiss()
        }

        builder.setNegativeButton(android.R.string.cancel) { dialog, p1 ->
            dialog.cancel()
        }

        builder.show()
    }

    private fun startButtonClicked(level: DifficultyLevels) {
        val intent = Intent(this, GameActivity::class.java)
        intent.putExtra(Constants.DIFFICULTY, level)
        startActivity(intent)
    }
}