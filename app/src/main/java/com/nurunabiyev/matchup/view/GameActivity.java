package com.nurunabiyev.matchup.view;

import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.kc.unsplash.Unsplash;
import com.kc.unsplash.models.Photo;
import com.kc.unsplash.models.SearchResults;
import com.nurunabiyev.matchup.Constants.GameFinishedResult;
import com.nurunabiyev.matchup.R;
import com.nurunabiyev.matchup.Utils;
import com.nurunabiyev.matchup.db.PlayerDatabase;
import com.nurunabiyev.matchup.game.GameManager;
import com.nurunabiyev.matchup.game.model.Player;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

import static com.nurunabiyev.matchup.Constants.DIFFICULTY;
import static com.nurunabiyev.matchup.Constants.DifficultyLevels;
import static com.nurunabiyev.matchup.Constants.GameFinishedResult.FINISH_NORMAL;
import static com.nurunabiyev.matchup.Constants.GameFinishedResult.HARD_LEVEL_PASSED;
import static com.nurunabiyev.matchup.Constants.GameFinishedResult.LEVEL_UNLOCKED;
import static com.nurunabiyev.matchup.Constants.GameFinishedResult.NEW_RECORD;
import static com.nurunabiyev.matchup.Constants.QUERY_CATS;
import static com.nurunabiyev.matchup.Constants.UNSPLASH_CLIENT_ID;

public class GameActivity extends AppCompatActivity {

    private int items = 2, cols = 2;
    private GameManager mGameManager;
    private DifficultyLevels diffLevel;
    private TilesAdapter mTilesAdapter;
    private Unsplash unsplash;
    private PlayerDatabase mDatabase;
    private TextView timeTV;
    private GridView gridView;
    private long secondsPassed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game);

        mDatabase = PlayerDatabase.Companion.getInstance(this);
        unsplash = new Unsplash(UNSPLASH_CLIENT_ID);

        createGameManager();

        gridView = findViewById(R.id.grid_view);
        timeTV = findViewById(R.id.tv_time);
        timeTV.setText(secondsString(0));
        gridView.setNumColumns(cols);

        downloadAndStart();
    }

    private void createGameManager() {
        diffLevel = (DifficultyLevels) getIntent().getSerializableExtra(DIFFICULTY);
        switch (diffLevel) {
            case EASY:
                items = 12; // 4x3
                cols = 3;
                break;
            case MEDIUM:
                items = 16; // 4x4
                cols = 4;
                break;
            case HARD:
                items = 24; // 6x4
                cols = 4;
                break;
        }

        mGameManager = new GameManager(items);
    }

    private void downloadAndStart() {
        if (!Utils.isConnected()) {
            showErrorAlert(getString(R.string.error_network), getString(R.string.error_no_internet));
        } else {
            unsplash.searchPhotos(QUERY_CATS, 1, items / 2, new Unsplash.OnSearchCompleteListener() {
                @Override
                public void onComplete(SearchResults results) {
                    ArrayList<Photo> photos = (ArrayList<Photo>) results.getResults();

                    // setting photos to tiles
                    for (int i = 0; i < photos.size(); i++) {
                        mGameManager.getGameBoard().setPhotoToTiles(photos.get(i).getUrls().getSmall());
                    }

                    mTilesAdapter = new TilesAdapter(GameActivity.this, mGameManager, results.getResults());
                    gridView.setAdapter(mTilesAdapter);

                    observeGameStatus();
                    gridView.setSelected(false);

                    observeTimeCounter();
                }

                @Override
                public void onError(String error) {
                    showErrorAlert(getString(R.string.error), R.string.error_something_wrong + error);
                }
            });
        }
    }

    private void showErrorAlert(String title, String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                downloadAndStart();
                dialog.cancel();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        builder.show();
    }

    private void observeGameStatus() {
        Observer<Boolean> gameStatusObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                ArrayList<Player> players = (ArrayList<Player>) mDatabase.playerDao().findAllPlayers();
                Player currentPlayer = players.get(0);

                // update player with new record
                GameFinishedResult gameResult = FINISH_NORMAL;
                switch (diffLevel) {
                    case EASY:
                        if (currentPlayer.getEasyScore() == 0) {
                            currentPlayer.setEasyScore((int) secondsPassed);
                            gameResult = LEVEL_UNLOCKED;
                        } else if (secondsPassed < currentPlayer.getEasyScore()) {
                            currentPlayer.setEasyScore((int) secondsPassed);
                            gameResult = NEW_RECORD;
                        }
                        break;
                    case MEDIUM:
                        if (currentPlayer.getMediumScore() == 0) {
                            currentPlayer.setMediumScore((int) secondsPassed);
                            gameResult = LEVEL_UNLOCKED;
                        } else if (secondsPassed < currentPlayer.getMediumScore()) {
                            currentPlayer.setMediumScore((int) secondsPassed);
                            gameResult = NEW_RECORD;
                        }
                        break;
                    case HARD:
                        if (currentPlayer.getHardScore() == 0) {
                            currentPlayer.setHardScore((int) secondsPassed);
                            gameResult = HARD_LEVEL_PASSED;
                        } else if (secondsPassed < currentPlayer.getHardScore()) {
                            currentPlayer.setHardScore((int) secondsPassed);
                            gameResult = NEW_RECORD;
                        }
                        break;
                }
                mDatabase.playerDao().updatePlayer(currentPlayer);

                congratulate(gameResult);

                mTilesAdapter.stopTimer();
            }
        };
        mGameManager.didPlayerWin.observe(this, gameStatusObserver);
    }

    private void congratulate(GameFinishedResult gameResult) {
        switch (gameResult) {
            case NEW_RECORD:
                Toasty.custom(this, getString(R.string.new_record), getResources().getDrawable(R.drawable.star_rating),
                        getResources().getColor(R.color.colorPrimary), Toast.LENGTH_LONG, true, true).show();
                break;
            case FINISH_NORMAL:
                Toasty.info(this, getString(R.string.try_better), Toast.LENGTH_LONG, true).show();
                break;
            case LEVEL_UNLOCKED:
                Toasty.success(this, getString(R.string.level_unlocked), Toast.LENGTH_LONG, true).show();
                break;
            case HARD_LEVEL_PASSED:
                Toasty.warning(this, getString(R.string.hard_level_passed), Toast.LENGTH_LONG, false).show();
                break;
        }
    }

    private void observeTimeCounter() {
        final Observer<Long> elapsedTimeObserver = new Observer<Long>() {
            @Override
            public void onChanged(@Nullable final Long aLong) {
                secondsPassed = aLong == null ? 0 : aLong;
                timeTV.setText(secondsString(secondsPassed));
            }
        };

        mTilesAdapter.getElapsedTime().observe(this, elapsedTimeObserver);
    }

    private Spanned secondsString(long seconds) {
        return Html.fromHtml(getResources().getString(R.string.seconds_passed)
                + " <font color=\"#FF4081\">" + seconds + "</font><br><br>");
    }
}